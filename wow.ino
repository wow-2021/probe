#include <Wire.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include <BH1750.h>
#include <Adafruit_VEML6070.h>
 
/***********************
Sunlight:       107,527
Full Daylight:  10,752
Overcast Day:   1,075
Very Dark Day:  107
Twilight:       10.8
Deep Twilight:  1.08
Full Moon:      .108
Quarter Moon:   .0108
Starlight:      .0011
Overcast Night: .0001
***********************/

#define SNESOR_SDA 23
#define SNESOR_SCL 22
#define SMS_SIGNAL 33

// https://tides4fishing.com/as/singapore/singapur/forecast/atmospheric-pressure
#define SEALEVELPRESSURE_HPA (1000) 

BH1750 lightMeter; //address: 0x23(0.7xVCC) or 0x5C (>0.7VCC)
Adafruit_BME280 bme; //address: 0x76
Adafruit_VEML6070 uvMeter = Adafruit_VEML6070();

const char* ssid = "berryfamily"; 
const char* password = "A85888133-0";
const char* mqtt_server = "192.168.1.58";
const int mqtt_port = 2883; 

String user_name = "mEyq6X41dacIGQKjgoma";
String clientID ="Anhe WoW probe corridor";

WiFiClient espClient;
PubSubClient psClient(espClient);
long lastMsg = 0;
int interval = 60000; // 60 x 1000ms = 1 minute

void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}

void setup() {
  Serial.begin(115200);

  initWiFi();
  Serial.print("RSSI: ");
  Serial.println(WiFi.RSSI());

  psClient.setServer(mqtt_server, mqtt_port);

  Wire.begin(SNESOR_SDA,SNESOR_SCL);
// configure BMP/BME 280 sensor
  if (!bme.begin(0x76, &Wire)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }

  // configure light sensor
//  BH1750::CONTINUOUS_LOW_RES_MODE
//  BH1750::CONTINUOUS_HIGH_RES_MODE
//  BH1750::CONTINUOUS_HIGH_RES_MODE_2
//  BH1750::ONE_TIME_LOW_RES_MODE
//  BH1750::ONE_TIME_HIGH_RES_MODE
//  BH1750::ONE_TIME_HIGH_RES_MODE_2
  lightMeter.configure(BH1750::ONE_TIME_HIGH_RES_MODE);
  lightMeter.begin();

  // configure UV sensor
  // UV index info: https://en.wikipedia.org/wiki/Ultraviolet_index
  uvMeter.begin(VEML6070_1_T, &Wire);
}

void reconnect() {
  // Loop until we're reconnected
  while (!psClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (psClient.connect(clientID.c_str(),user_name.c_str(),NULL)) {
      Serial.println("connected");
      sendAttr();
    } else {
      Serial.print("failed, rc=");
      Serial.print(psClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {
  long now = millis();
//  if (now - lastMsg > interval) {
    lastMsg = now;
    if (!psClient.connected()) {
      reconnect();
    }
    psClient.loop();
  
    float lux = lightMeter.readLightLevel();
    Serial.println(lux);

    int uv = uvMeter.readUV();
    Serial.println(uv);
    
    float soilmoisture = analogRead(SMS_SIGNAL)*100.00/4095.00;
    
    float temperature = bme.readTemperature();
    float humidity = bme.readHumidity();
    float pressure = bme.readPressure() / 100.0F;
    float altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
    Serial.println(temperature);

    int ADC_VALUE = analogRead(35);
    int adc_percentage = int(100 * ADC_VALUE / 4095);
    float voltage_value = (ADC_VALUE * 3.3 ) / (4095);
    
    updateReading(lux, temperature, humidity, pressure, altitude, uv, soilmoisture, ADC_VALUE, adc_percentage, voltage_value);
//  }
  long needWait = interval - (millis() - now);
  if (needWait < 0){
    needWait = 0;
  }
  delay(needWait);
}

void updateReading(float lux, float temperature, float humidity, float pressure, float altitude, int uv, float soilmoisture, int ADC_VALUE, int adc_percentage, float voltage_value) {
  String msg = "{";
  msg += "\"light\":";
  msg += String(lux);
  msg += ",";
  msg += "\"temperature\":";
  msg += String(temperature);
  msg += ",";
  msg += "\"humidity\":";
  msg += String(humidity);
  msg += ",";
  msg += "\"pressure\":";
  msg += String(pressure);
  msg += ",";
  msg += "\"altitude\":";
  msg += String(altitude);
  msg += ",";
  msg += "\"uv\":";
  msg += String(uv);
  msg += ",";
  msg += "\"soilmoisture\":";
  msg += String(soilmoisture);
  msg += ",";
  msg += "\"adc_value\":";
  msg += String(ADC_VALUE);
  msg += ",";
  msg += "\"adc_percentage\":";
  msg += String(adc_percentage);
  msg += ",";
  msg += "\"voltage_value\":";
  msg += String(voltage_value);
  msg += "}";
  psClient.publish("v1/devices/me/telemetry", msg.c_str());
}

void sendAttr() {
  String msg_attribute ="{";  
  msg_attribute += "\"mac\":";
  msg_attribute += "\"" + String(WiFi.macAddress())+ "\"";
  msg_attribute += ",";
  msg_attribute += "\"ip\":";
  msg_attribute += "\"" + WiFi.localIP().toString() + "\"";
  msg_attribute += ",";
  msg_attribute += "\"rssi\":";
  msg_attribute += "\"" + String(WiFi.RSSI()) + "\"";
  msg_attribute += "}";
  psClient.publish("v1/devices/me/attributes" ,msg_attribute.c_str());
}
